package com.example.controltest;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

public class Activity_Time extends AppCompatActivity {

    private Button btTime;
    private EditText mEtTime;
    private TimePickerDialog.OnTimeSetListener onTimeSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__time);

        btTime = findViewById(R.id.btTimeClick);
        mEtTime = findViewById(R.id.et_showTime);

        btTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar calendar = Calendar.getInstance();
                int hour = Calendar.HOUR;
                int minute = Calendar.MINUTE;

                TimePickerDialog timePickerDialog= new TimePickerDialog(Activity_Time.this, onTimeSetListener,hour,minute,
                        true);


            }
        });
    }
}
