package com.example.controltest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {


    private RadioButton mrdFemale;
    private RadioButton mrdMale;
    private Button mbtSend;
    private RadioGroup rgGender;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radio_test);

        mrdFemale = findViewById(R.id.rdFemale);
        mrdMale = findViewById(R.id.rdMale);
        mbtSend = findViewById(R.id.btSend);
        rgGender = findViewById(R.id.radioGroup);

        mbtSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int idCheck = rgGender.getCheckedRadioButtonId();
//                String gender= "";
                Intent intent = new Intent(MainActivity.this, Activity_Result.class);
                startActivity(intent);
            }
        });
    }
}
